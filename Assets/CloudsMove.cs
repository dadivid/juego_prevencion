﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsMove : MonoBehaviour
{
   public List<Transform> clouds = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        clouds.Add(transform.GetChild(5).GetChild(0).transform);
        clouds.Add(transform.GetChild(5).GetChild(1).transform);
        clouds.Add(transform.GetChild(5).GetChild(2).transform);
        clouds.Add(transform.GetChild(5).GetChild(3).transform);
        clouds.Add(transform.GetChild(5).GetChild(4).transform);

    }

    // Update is called once per frame
    void Update()
    {
        clouds[0].transform.position = new Vector2(clouds[0].transform.position.x - 0.5f * Time.deltaTime, clouds[0].transform.position.y);
        clouds[1].transform.position = new Vector2(clouds[1].transform.position.x - 0.4f * Time.deltaTime, clouds[1].transform.position.y);
        clouds[2].transform.position = new Vector2(clouds[2].transform.position.x - 0.3f * Time.deltaTime, clouds[2].transform.position.y);
        clouds[3].transform.position = new Vector2(clouds[3].transform.position.x - 0.2f * Time.deltaTime, clouds[3].transform.position.y);
        clouds[4].transform.position = new Vector2(clouds[4].transform.position.x - 0.1f * Time.deltaTime, clouds[4].transform.position.y);

        foreach (Transform item in clouds)
        {
            if( item.transform.position.x<-20)
            {
               item.transform.position = new Vector3(20, transform.position.y, transform.position.z);
            }
        }
    }
}
