﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsPanel : MonoBehaviour
{   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetType(int typeValue)
    {
        switch(typeValue)
        {
            case 0: TypeGood();
                break;
            case 1:
                TypeBad();
                break;
            case 2:
                TyperBonus();
                break;
        }
    }

    public void TypeGood()
    {
        Disparar.m_TypeOfBullet = TypeOfMessage.Good;
    }
    public void TypeBad()
    {
        Disparar.m_TypeOfBullet = TypeOfMessage.Bad;
    }
    public void TyperBonus()
    {
        Disparar.m_TypeOfBullet = TypeOfMessage.Bonus;
    }
        
    public static TypeOfMessage GetTypeOfMessageFromIndex(int index)
    {
        switch (index)
        {
            case 0:
                return TypeOfMessage.Good;
            case 1:
                return TypeOfMessage.Bad;
            case 2:
                return TypeOfMessage.Bonus;
        }

        return TypeOfMessage.Bad;
    }
}
