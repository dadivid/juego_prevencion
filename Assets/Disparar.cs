﻿using Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Disparar : MonoBehaviour {

    [Header("Bullet Options")]
    public GameObject m_bullet;
    public static bool m_IsShootingActive = true;
    public static TypeOfMessage m_TypeOfBullet = TypeOfMessage.Bad;
    private float m_BaseAngleLimit = 10f;
    private GameObject m_CurrentBullet;
    public Transform m_BulletReference;
    private Vector3 m_LastGundirection = Vector3.up;

    [Header("Guide Lines")]
    public static bool m_GuideLineActive = true;
    private float m_MaxDistanceGuideLine = 10.0f;
    private List<GameObject> m_GuideLinePoints;
    private int m_GuideLinePointsCount = 10;
    private float m_DistanceBetweenPoints;
    public GameObject m_GuideLinePoint;

    private float m_BulletRadio = 0.5f;

    [Header("Bullet Buttons")]
    public Transform m_BulletButtonsReference;
    public GameObject[] m_BulletButtons;
    public List<GameObject> m_ListOfButtons;
    public static int m_CurrentButtonIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_IsShootingActive = true;
        m_GuideLineActive = true;
        m_CurrentButtonIndex = 0;

        m_GuideLinePoints = new List<GameObject>();
        for(int i = 0; i < m_GuideLinePointsCount; i++)
        {
            GameObject glpoint = (GameObject)Instantiate(m_GuideLinePoint, transform.position, Quaternion.identity, transform);
            m_GuideLinePoints.Add(glpoint);
        }
        m_DistanceBetweenPoints = m_MaxDistanceGuideLine / m_GuideLinePointsCount;

        m_ListOfButtons = new List<GameObject>();
        for (int i = 0; i < m_BulletButtons.Length; i++)
        {
            GameObject bb = (GameObject)Instantiate(m_BulletButtons[i], m_BulletButtonsReference);            
            bb.GetComponent<Button>().onClick.AddListener(delegate { m_BulletButtonsReference.GetComponent<ButtonsPanel>().SetType(i); });
            bb.name = i.ToString();
            bb.GetComponent<BulletButton>().m_TypeOfMessage = ButtonsPanel.GetTypeOfMessageFromIndex(i);
            m_ListOfButtons.Add(bb);
        }

        m_CurrentBullet = (GameObject) Instantiate(m_bullet, transform.position, transform.rotation);
        m_CurrentBullet.transform.parent = transform;
        m_CurrentBullet.GetComponent<BulletMovement>().SetTypeOfMessage(m_TypeOfBullet);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameController.m_IsGameActive)
        {
            LookToPointer();
            ShootBullet();
            CreateGuideLine();
        }
    }

    public void LookToPointer()
    {
        Vector3 gunDirection;
#if UNITY_EDITOR || UNITY_WEBGL
        gunDirection = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
#elif UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            gunDirection = new Vector3(t.position.x, t.position.y, 0) - Camera.main.WorldToScreenPoint(transform.position);
            m_LastGundirection = gunDirection;
        }      
        else
        {                        
            gunDirection = m_LastGundirection;
        }
#endif
        float radiansAngle = Mathf.Atan(gunDirection.y / gunDirection.x);
        radiansAngle = radiansAngle < 0 ? radiansAngle + Mathf.PI : radiansAngle;

        //Avoid to rotate the gun less than the horizontal limits
        if (gunDirection.y > m_BaseAngleLimit)
        {
            transform.eulerAngles = new Vector3(0, 0, radiansAngle * 180.0f / Mathf.PI - 90.0f);
        }
    }

    public void ShootBullet()
    {
#if UNITY_EDITOR || UNITY_WEBGL
        if (Input.GetMouseButtonDown(1) && m_IsShootingActive == true)
        {
            if(Input.mousePosition.y > Camera.main.WorldToScreenPoint(transform.position).y)
            {
                m_CurrentBullet.GetComponent<BulletMovement>().StartMovement();
                m_CurrentBullet.transform.parent = m_BulletReference;
                m_IsShootingActive = false;

                UpdateListOfButtons(m_CurrentButtonIndex);
            }
            
        }
#elif UNITY_ANDROID
        if(Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            if(t.position.y > Camera.main.WorldToScreenPoint(transform.position).y)
            {
                if (t.phase == TouchPhase.Ended && m_IsShootingActive == true)
                {
                    m_CurrentBullet.GetComponent<BulletMovement>().StartMovement();
                    m_CurrentBullet.transform.parent = m_BulletReference;
                    m_IsShootingActive = false;

                    UpdateListOfButtons(m_CurrentButtonIndex);
                }
            }
        }
        
#endif
    }

    public void UpdateListOfButtons(int index)
    {
        Destroy(m_ListOfButtons[index]);
        m_ListOfButtons.RemoveAt(index);

        for(int i = 0; i < m_ListOfButtons.Count; i++)
        {
            m_ListOfButtons[i].name = i.ToString();
        }
        CreateBulletButton();
    }

    public void CreateBulletButton()
    {
        int bulletButton = UnityEngine.Random.Range(0, m_BulletButtons.Length);
        GameObject bb = (GameObject)Instantiate(m_BulletButtons[bulletButton], m_BulletButtonsReference);
        bb.GetComponent<Button>().onClick.AddListener(delegate { m_BulletButtonsReference.GetComponent<ButtonsPanel>().SetType(bulletButton); });
        bb.name = m_ListOfButtons.Count.ToString();
        bb.GetComponent<BulletButton>().m_TypeOfMessage = ButtonsPanel.GetTypeOfMessageFromIndex(bulletButton);
        m_ListOfButtons.Add(bb);

        //Create new Bullet
        m_TypeOfBullet = m_ListOfButtons[0].GetComponent<BulletButton>().m_TypeOfMessage;
        m_CurrentBullet = (GameObject)Instantiate(m_bullet, transform.position, transform.rotation);
        m_CurrentBullet.transform.parent = transform;
        m_CurrentBullet.GetComponent<BulletMovement>().SetTypeOfMessage(m_TypeOfBullet);
    }

    public void ChangeColorCurrentBullet(TypeOfMessage type)
    {
        m_CurrentBullet.GetComponent<BulletMovement>().SetTypeOfMessage(type);
    }

    public void CreateGuideLine()
    {
        if(m_GuideLineActive)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, m_MaxDistanceGuideLine, 1 << LayerMask.NameToLayer("BorderSide"));
            if (hit.collider != null)
            {
                ShowGuideLinePoints();
                
                float distance = Mathf.Abs(Vector3.Distance(hit.point, transform.position));
                int firstDirectionTotalPoints = (int)(Mathf.Floor(distance / m_DistanceBetweenPoints));
                int difference = m_GuideLinePointsCount - firstDirectionTotalPoints;

                //FirstLine
                CreateGuideLine(transform.position, transform.up, firstDirectionTotalPoints, 0, Color.white);

                //SecondLine
                Vector2 offsetX = transform.up.x > 0 ? new Vector2(-m_BulletRadio, 0) : new Vector2(m_BulletRadio, 0);
                CreateGuideLine(hit.point + 2 * offsetX, new Vector3(-transform.up.x, transform.up.y, transform.up.z),
                                                        difference, firstDirectionTotalPoints, Color.white);
            }
            else
            {
                CreateGuideLine(transform.position, transform.up, m_GuideLinePointsCount, 0, Color.white);
                //HideGuideLinePoints();
            }
        }
    }

    private void CreateGuideLine(Vector3 startPoint, Vector3 direction, int totalPoints, int baseIndex, Color color)
    {
        int total = baseIndex + totalPoints > m_GuideLinePointsCount ? m_GuideLinePointsCount : baseIndex + totalPoints;
        for (int i = baseIndex; i < total; i++)
        {
            m_GuideLinePoints[i].transform.position = startPoint + (i - baseIndex) * direction;
            m_GuideLinePoints[i].GetComponentInChildren<SpriteRenderer>().color = color;
        }
    }

    private void HideGuideLinePoints()
    {
        for (int i = 0; i < m_GuideLinePointsCount; i++)
        {
            m_GuideLinePoints[i].SetActive(false);
        }
    }

    private void ShowGuideLinePoints()
    {
        for (int i = 0; i < m_GuideLinePointsCount; i++)
        {
            m_GuideLinePoints[i].SetActive(true);
        }
    }

    void ResetShooting()
    {
        m_IsShootingActive = false;
    }

    public void DeactiveCurrentBullet()
    {
        m_CurrentBullet.SetActive(false);
    }
}
