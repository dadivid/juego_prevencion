﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletButton : MonoBehaviour
{
    private Disparar m_DispararPanel;
    [HideInInspector]
    public TypeOfMessage m_TypeOfMessage;

    private void Start()
    {
        m_DispararPanel = FindObjectOfType<Disparar>();
        GetComponent<Button>().onClick.AddListener(SelectedButton);
        //Disparar.m_CurrentButtonIndex = int.Parse(name);
    }

    public void SelectedButton()
    {
        m_DispararPanel.ChangeColorCurrentBullet(m_TypeOfMessage);
    }
}
