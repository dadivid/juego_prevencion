﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class BulletMovement : MonoBehaviour
{
    public TypeOfMessage m_TypeOfMessage;
    private Rigidbody2D m_RigidBody;
    private Collider2D m_Collider;
    public float m_Speed = 10.0f;

    AudioSource m_SFX;
    public AudioClip m_Shooting;
    public AudioClip m_poping;
    public AudioClip m_Bounce;
    public AudioClip m_wrong;
    public AudioClip m_wrongLaught;

    [Header("Bullet Explosion")]
    public GameObject m_BulletExplosion;

    private GameObject m_CurrentCollisionObject;

    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Collider = GetComponent<Collider2D>();
        m_Collider.isTrigger = true;
        m_SFX = GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(0).GetComponent<AudioSource>();
        m_SFX.PlayOneShot(m_Shooting);
        //StartMovement();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartMovement()
    {
        m_Collider.isTrigger = false;
        m_RigidBody.velocity = transform.up * m_Speed;
        //Destroy(gameObject, 1.5f);
        //Invoke("ResetDisparar", 1f);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "MessageBubble")
        {

            GameController GC = FindObjectOfType<GameController>();
            ResetDisparar();
            m_SFX.PlayOneShot(m_poping);
            //m_RigidBody.velocity = new Vector2(-m_RigidBody.velocity.x, m_RigidBody.velocity.y);
            if (col.gameObject.GetComponent<MessageBubble>().m_Message.m_TypeOfMessage == m_TypeOfMessage)
            {
                if (MessageController.m_MessagesCont > 5)
                {
                    GC.DamageMonster();
                }
            }
           else
            {
                if (MessageController.m_MessagesCont > 5)
                {
                    GC.DamagePlayer();
                }
                m_SFX.PlayOneShot(m_wrong);
                GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(1).GetComponent<AudioSource>().PlayOneShot(m_wrongLaught);
            }

            GameObject exp = (GameObject)Instantiate(m_BulletExplosion, transform.position, transform.rotation);
            exp.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;

            m_RigidBody.velocity = Vector2.zero;
            m_RigidBody.isKinematic = true;
            col.transform.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            col.transform.GetComponent<Rigidbody2D>().isKinematic = true;
            //var messageBubbles = FindObjectsOfType<MessageBubble>();
            //foreach (var m in messageBubbles)
            //{
            //    m.StopMovement();
            //}
            col.transform.GetComponent<Animator>().SetTrigger("increase");

            m_CurrentCollisionObject = col.gameObject;
            GameController.m_IsGameActive = false;
            StartCoroutine(DestroyInvoke());
        }
        else
        {
            m_SFX.PlayOneShot(m_Bounce);
        }
    }

    IEnumerator DestroyInvoke()
    {
        yield return new WaitForSeconds(1.0f);
        GameController.m_IsGameActive = true;
        //var messageBubbles = FindObjectsOfType<MessageBubble>();
        //foreach (var m in messageBubbles)
        //{
        //    m.ContinueMovement();
        //}
        Destroy(m_CurrentCollisionObject);
        Destroy(gameObject);
    }

    public void SetTypeOfMessage(TypeOfMessage type)
    {        
        m_TypeOfMessage = type;
        if (m_TypeOfMessage == TypeOfMessage.Good)
        {
            GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if (m_TypeOfMessage == TypeOfMessage.Bad)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            Color32 purple = new Color32(213, 115, 255, 255);
            GetComponent<SpriteRenderer>().color = purple;
        }
        transform.GetChild(1).GetComponent<Light2D>().color = GetComponent<SpriteRenderer>().color;
    }

    public void ResetDisparar()
    {
        Disparar.m_IsShootingActive = true;
    }
}
