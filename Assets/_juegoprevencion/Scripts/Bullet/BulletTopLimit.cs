﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTopLimit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag == "Bullet")
        {
            col.GetComponent<BulletMovement>().ResetDisparar();
            Destroy(col.gameObject);
        }

        if (col.transform.tag == "MessageBubble")
        {
            col.isTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.transform.tag == "Bullet")
        {
            col.GetComponent<BulletMovement>().ResetDisparar();
            Destroy(col.gameObject);
        }

        if (col.transform.tag == "MessageBubble")
        {
            col.isTrigger = false;
        }
    }
}
