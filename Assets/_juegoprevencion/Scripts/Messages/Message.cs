﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public enum TypeOfMessage
    {
        Good,
        Bad,
        Bonus
    }

    [System.Serializable]
    public class Message
    {
        public string m_Message;
        public TypeOfMessage m_TypeOfMessage;
    }
}
