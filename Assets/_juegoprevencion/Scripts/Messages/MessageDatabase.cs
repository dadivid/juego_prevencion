﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MessageDatabaseObject", order = 1)]
    public class MessageDatabase : ScriptableObject
    {
        public List<Message> m_Message;
    }
}