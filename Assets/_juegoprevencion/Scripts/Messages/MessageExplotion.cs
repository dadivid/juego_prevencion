﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageExplotion : MonoBehaviour
{
    public Image m_ExplosionImage;

    private void Start()
    {
        Destroy(gameObject, 0.5f);
    }

    public void DestroyItself()
    {
        
    }
}
