﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageController : MonoBehaviour
{
    public MessageDatabase m_MessageDatabaseObject;
    public Transform[] m_InstatiatePositions;
    public GameObject m_MessageBubble;
    private int m_MessageIndex = 0;

    public static bool m_IsMessagingCreationActive = true;

    public static int m_MessagesCont;

    public float m_TimeToWait = 4;

    // Start is called before the first frame update
    void Start()
    {
        m_MessagesCont = 0;
        m_IsMessagingCreationActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(m_IsMessagingCreationActive + "   " + GameController.m_IsGameActive);
        if (m_IsMessagingCreationActive && GameController.m_IsGameActive)
        {
            StartMessageCreation();
        }
    }

    public Message GetMessageByIndex(int messageIndex)
    {
        return m_MessageDatabaseObject.m_Message[messageIndex];
    }

    public void CreateBubbleMessage()
    {
        GameObject mb = (GameObject)Instantiate(m_MessageBubble, m_InstatiatePositions[Random.Range(0, m_InstatiatePositions.Length)].position, Quaternion.identity, transform);
        mb.GetComponent<MessageBubble>().m_Message = GetMessageByIndex(m_MessageIndex);
        mb.GetComponent<MessageBubble>().UpdateUIMessage();
        m_MessageIndex++;
        m_MessageIndex = m_MessageIndex >= m_MessageDatabaseObject.m_Message.Count ? m_MessageIndex = 0 : m_MessageIndex;

        m_MessagesCont++;
    }

    private void StartMessageCreation()
    {
        m_IsMessagingCreationActive = false;
        StartCoroutine(MessageCreation());
    }

    IEnumerator MessageCreation()
    {
        CreateBubbleMessage();
        yield return new WaitForSeconds(m_TimeToWait);
        m_IsMessagingCreationActive = true;        
    }
}
