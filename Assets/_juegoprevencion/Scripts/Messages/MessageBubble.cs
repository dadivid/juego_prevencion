﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Messages
{
    public class MessageBubble : MonoBehaviour
    {
        //[HideInInspector]
        public Message m_Message;
        public Text m_UIText;
        public float m_Speed = 1.0f;

        private Rigidbody2D m_RigidBody;

        public GameObject m_MessageExplotion;
        public Outline m_MesssageImage;

        private void Start()
        {
            m_RigidBody = GetComponent<Rigidbody2D>();
            m_RigidBody.velocity = Vector2.down * m_Speed;
        }

        public void UpdateUIMessage()
        {
            m_UIText.text = m_Message.m_Message;
            SetTypeOfMessage();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.transform.tag == "Bullet")
            {
                //m_RigidBody.gravityScale = 1f;
                // m_RigidBody.velocity = Vector2.zero;
                // m_RigidBody.velocity = Vector2.down * m_Speed;
                // m_RigidBody.velocity = Vector2.zero;
                //TODO - Verfify if result is good or not
                GameObject exp = (GameObject)Instantiate(m_MessageExplotion, transform.position, transform.rotation);
                //Debug.Log(exp.GetComponent<MessageExplotion>().m_ExplosionImage.color);
                exp.GetComponent<MessageExplotion>().m_ExplosionImage.color = m_MesssageImage.effectColor;
                //Debug.Log(exp.GetComponent<MessageExplotion>().m_ExplosionImage.color);
            }
            if(collision.transform.tag=="Borderbase")
            {
                FindObjectOfType<GameController>().DamagePlayer();
                Destroy(gameObject);
            }
        }

        public void StopMovement()
        {
            m_RigidBody.velocity = Vector2.zero;
        }

        public void ContinueMovement()
        {
            m_RigidBody.velocity = Vector2.down * m_Speed;
        }

        public void SetTypeOfMessage()
        {
            if (m_Message.m_TypeOfMessage == TypeOfMessage.Good)
            {
                if(MessageController.m_MessagesCont < 4)
                {
                    m_MesssageImage.effectColor = Color.green;
                }
               
            }
            else if (m_Message.m_TypeOfMessage == TypeOfMessage.Bad)
            {
                if (MessageController.m_MessagesCont < 4)
                {
                    m_MesssageImage.effectColor = Color.red;
                }
            }
            else
            {
                if (MessageController.m_MessagesCont < 4)
                {
                    Color32 purple = new Color32(213, 115, 255, 255);
                    m_MesssageImage.effectColor = purple;
                }
            }
        }
    }
}
