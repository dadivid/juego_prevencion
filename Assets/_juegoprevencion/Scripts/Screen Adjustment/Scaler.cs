﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScreenAdjusment
{
    public class Scaler : MonoBehaviour
    {
        public enum TypeOfAdjustment
        {
            ByHeight,
            ByWidth
        }

        public TypeOfAdjustment m_TypeOfAdjustment;

        // Start is called before the first frame update
        void Start()
        {
            if (m_TypeOfAdjustment == TypeOfAdjustment.ByHeight)
            {
                float height = ScreenSize.GetScreenToWorldHeight;
                transform.localScale = Vector3.one * height;
            }
            else
            {
                float width = ScreenSize.GetScreenToWorldWidth;
                transform.localScale = Vector3.one * width;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
