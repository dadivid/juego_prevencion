﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMonster : GenericCharacter
{
    [Header("MonsterAnimation")]
    public Animator m_Animator;
    public Animator m_Animator2;

    [Header("Final Animation")]
    public EndingAnimation m_EndingAnimation;

    [Header("Body Parts")]
    public SpriteRenderer[] m_MonsterParts;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void LoseGameAction()
    {
        base.LoseGameAction();
        GameController.m_IsGameActive = false;
        Disparar.m_IsShootingActive = false;

        var messageBubbles = FindObjectsOfType<MessageBubble>();
        foreach (var m in messageBubbles)
        {
            m.StopMovement();
        }
        FindObjectOfType<GameController>().m_MonsterLight2D.enabled = false;
        FindObjectOfType<Disparar>().DeactiveCurrentBullet();
        Invoke("ActiveMonsterAnimation", 2.0f);
    }

    public void ActiveMonsterAnimation()
    {
        ActiveAnimation("angry", true);
        Debug.Log("Ganaste");
        Invoke("ActiveEndingAnimation", 2.0f);
    }

    public void ActiveEndingAnimation()
    {
        m_EndingAnimation.m_AnimationState = "win";
        GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(3).GetComponent<AudioSource>().volume = 0.25f;
        GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(4).GetComponent<AudioSource>().Play();
        m_EndingAnimation.MoveToDown();
        Invoke("ActiveFinalMessage", 3.0f);
    }

    public override void ActiveAnimation(string name, bool? val)
    {
        base.ActiveAnimation(name, val);
        //m_Animator.SetBool(name, val);
        if (val == null)
        {
            m_Animator.SetTrigger(name);
            m_Animator2.SetTrigger(name);
        }
        else
        {
            m_Animator.SetBool(name, (bool)val);
            m_Animator2.SetBool(name, (bool)val);
        }
    }

    public void ActiveFinalMessage()
    {
        FindObjectOfType<GameController>().ActiveFinalMessage(true);
    }

    public override void ModifyAlphaColor(float health, float iniHealth)
    {
        Debug.Log(health + "   " + iniHealth);
        switch (m_HealthType)
        {
            case TypeOfMeasure.Bar:
                float alphaValue = health / iniHealth;
                for (int i = 0; i < m_MonsterParts.Length; i++)
                {
                    m_MonsterParts[i].color = new Color(m_MonsterParts[i].color.r, m_MonsterParts[i].color.g, m_MonsterParts[i].color.b, 1.0f - alphaValue);
                }
                break;
            case TypeOfMeasure.Steps:
                float alphaValue2 = health / iniHealth;
                Debug.Log(alphaValue2);
                for (int i = 0; i < m_MonsterParts.Length; i++)
                {
                    m_MonsterParts[i].color = new Color(m_MonsterParts[i].color.r, m_MonsterParts[i].color.g, m_MonsterParts[i].color.b, 1.0f - alphaValue2);
                }
                break;
        }

        
    }
}
