﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericPlayer : GenericCharacter
{
    [Header("Game Elements")]
    public MessageController m_MessageController;
    public EndingAnimation m_EndingAnimation;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            IncreaseHealth();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReduceHealth();
        }
    }

    public override void LoseGameAction()
    {
        base.LoseGameAction();
        GameController.m_IsGameActive = false;
        Disparar.m_IsShootingActive = false;

        var messageBubbles = FindObjectsOfType<MessageBubble>();
        foreach(var m in messageBubbles)
        {
            m.StopMovement();
        }

        FindObjectOfType<GameController>().m_Monster.ActiveAnimation("laugh", true);
        FindObjectOfType<GameController>().m_PlayerLight2D[PlayerPrefs.GetInt("genre")].enabled = false;
        FindObjectOfType<Disparar>().DeactiveCurrentBullet();
        Invoke("ActiveEndingAnimation", 2.0f);
    }

    public void ActiveEndingAnimation()
    {
        GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(3).GetComponent<AudioSource>().Stop();
        GameObject.FindGameObjectWithTag("SoundController").transform.GetChild(5).GetComponent<AudioSource>().Play();
        m_EndingAnimation.m_AnimationState = "lose";
        m_EndingAnimation.MoveToDown();
        Invoke("ActiveFinalMessage", 3.0f);
    }

    public void ActiveFinalMessage()
    {
        FindObjectOfType<GameController>().ActiveFinalMessage(false);
    }
}
