﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericCharacter : MonoBehaviour
{
    public enum TypeOfMeasure
    {
        Bar,
        Steps
    }

    [Header("Power")]
    public TypeOfMeasure m_PowerType;
    public float m_Power = 1.0f;
    private float m_PowerInitialValue;
    public GameObject m_VisualPower;
    public int m_NumberOfPowerSteps = 5;
    private List<GameObject> m_ListOfPowerSteps;
    public GameObject m_VisualPowerRoot;
    public GameObject m_VisualPowerStep; //element of a grid
    public float m_PowerDelta = 1.0f;

    [Header("Health")]
    public TypeOfMeasure m_HealthType;
    public float m_Health = 100.0f;
    protected float m_HealthInitialValue;
    public GameObject m_VisualHealth;
    public int m_NumberOfHealthSteps = 5;
    protected List<GameObject> m_ListOfHealthSteps;
    public GameObject m_VisualHealthRoot;
    public GameObject m_VisualHealthStep; //element of a grid
    public float m_Damage = 10.0f;

    // Start is called before the first frame update
    protected void Start()
    {
        m_ListOfPowerSteps = new List<GameObject>();
        m_ListOfHealthSteps = new List<GameObject>();
        m_PowerInitialValue = 100.0f - m_Power;
        m_HealthInitialValue = m_Health;

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize()
    {
        switch (m_PowerType)
        {
            case TypeOfMeasure.Bar:
                m_VisualPower.GetComponent<Image>().fillAmount = 0.0f;
                break;
            case TypeOfMeasure.Steps:
                m_VisualPower.GetComponent<Image>().fillAmount = 0.0f;
                break;
        }

        switch (m_HealthType)
        {
            case TypeOfMeasure.Bar:
                m_VisualHealth.GetComponent<Image>().fillAmount = 1.0f;
                break;
            case TypeOfMeasure.Steps:
                m_VisualHealth.GetComponent<Image>().fillAmount = 0.0f;
                for (int i = 0; i < m_NumberOfPowerSteps; i++)
                {
                    GameObject healthStep = (GameObject)Instantiate(m_VisualHealthStep, m_VisualHealthRoot.transform);
                    m_ListOfHealthSteps.Add(healthStep);
                }
                m_HealthInitialValue = (float)m_ListOfHealthSteps.Count;
                break;
        }

    }

    public void IncreasePower()
    {        
        switch(m_PowerType)
        {
            case TypeOfMeasure.Bar:
                m_Power = m_Power + m_PowerDelta > 100.0f ? 100.0f : m_Power + m_PowerDelta;
                m_VisualPower.GetComponent<Image>().fillAmount = m_Power / m_PowerInitialValue;
                break;
            case TypeOfMeasure.Steps:
                if (m_ListOfPowerSteps.Count < m_NumberOfPowerSteps)
                {
                    GameObject powerStep = (GameObject)Instantiate(m_VisualPowerStep, m_VisualPowerRoot.transform);
                    m_ListOfPowerSteps.Add(powerStep);
                }

                break;
        }  
    }


    public void ReducePower()
    {
        switch (m_PowerType)
        {
            case TypeOfMeasure.Bar:
                m_Power = m_Power - m_PowerDelta < 0.0f ? 0.0f : m_Power - m_PowerDelta;
                m_VisualPower.GetComponent<Image>().fillAmount = m_Power / m_PowerInitialValue;
                break;
            case TypeOfMeasure.Steps:
                if (m_ListOfPowerSteps.Count > 0)
                {
                    int last = m_ListOfPowerSteps.Count - 1;
                    Destroy(m_ListOfPowerSteps[last]);
                    m_ListOfPowerSteps.RemoveAt(last);
                }
                break;
        }
    }

    public void IncreaseHealth()
    {
        switch (m_HealthType)
        {
            case TypeOfMeasure.Bar:
                m_Health = m_Health + m_Damage < 100.0f ? m_Health + m_Damage : 100.0f;
                m_VisualHealth.GetComponent<Image>().fillAmount = m_Health / m_HealthInitialValue;
                break;
            case TypeOfMeasure.Steps:
                if (m_ListOfHealthSteps.Count < m_NumberOfHealthSteps)
                {
                    GameObject powerStep = (GameObject)Instantiate(m_VisualHealthStep, m_VisualHealthRoot.transform);
                    m_ListOfHealthSteps.Add(powerStep);
                    m_Health = (float)m_ListOfHealthSteps.Count;
                }
                break;
        }
    }

    public virtual void ReduceHealth()
    {
        
        switch (m_HealthType)
        {
            case TypeOfMeasure.Bar:
                m_Health = m_Health - m_Damage > 0.0f ? m_Health - m_Damage : 0.0f;
                m_VisualHealth.GetComponent<Image>().fillAmount = m_Health / m_HealthInitialValue;
                if (m_Health <= 0)
                {
                    LoseGameAction();
                }
                break;
            case TypeOfMeasure.Steps:
                if (m_ListOfHealthSteps.Count > 0)
                {
                    int last = m_ListOfHealthSteps.Count - 1;
                    Destroy(m_ListOfHealthSteps[last]);
                    m_ListOfHealthSteps.RemoveAt(last);
                    m_Health = (float)m_ListOfHealthSteps.Count;
                    if (m_ListOfHealthSteps.Count == 0)
                    {
                        LoseGameAction();
                    }
                }
                break;
        }

        
    }

    public void ResetValues()
    {
        m_Power = m_PowerInitialValue;
        m_Health = m_HealthInitialValue;
    }

    public float GetInitialHealth()
    {
        return m_HealthInitialValue;
    }

    public virtual void LoseGameAction() { }
    public virtual void ActiveAnimation(string name, bool? val) { }
    public virtual void ModifyAlphaColor(float health, float iniHealth) { }
}
