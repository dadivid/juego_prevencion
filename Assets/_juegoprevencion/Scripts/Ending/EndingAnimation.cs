﻿using ScreenAdjusment;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingAnimation : MonoBehaviour
{
    private float m_TopPosition;
    private float m_DownPosition;

    private float m_YPosition;

    public bool m_IsMovingDown = false;
    public bool m_IsMovingUp = false;

    private SpriteRenderer m_SpriteRenderer;
    private float m_MoveDownSpeed = 5.0f;
    private float m_MoveUpSpeed = 10.0f;

    [HideInInspector]
    public string m_AnimationState;
    public Animator[] m_FinalAnimation;
    
    //Temp solution
    public Animator m_FinalAnimation2;

    // Start is called before the first frame update
    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.enabled = false;
        Invoke("StartPosition", 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(m_IsMovingDown)
        {
            m_YPosition = Mathf.Lerp(m_YPosition, m_DownPosition, Time.deltaTime * m_MoveDownSpeed);
            transform.position = new Vector3(0, m_YPosition, 0);

            if(Mathf.Abs(m_YPosition - m_DownPosition) <= 0.01f)
            {
                if (m_AnimationState == "lose")
                {
                    m_FinalAnimation[PlayerPrefs.GetInt("genre")].gameObject.SetActive(true);
                    m_FinalAnimation[PlayerPrefs.GetInt("genre")].SetTrigger(m_AnimationState);
                }
                else
                {
                    m_FinalAnimation2.gameObject.SetActive(true);
                    m_FinalAnimation2.SetTrigger(m_AnimationState);
                }
                
                m_IsMovingDown = false;
                Invoke("ReloadGame", 20.0f);
            }
        }
        else if (m_IsMovingUp)
        {
            m_YPosition = Mathf.Lerp(m_YPosition, m_TopPosition, Time.deltaTime * m_MoveUpSpeed);
            transform.position = new Vector3(0, m_YPosition, 0);
        }
    }

    public void StartPosition()
    {
        m_TopPosition = transform.localScale.y / 2 + ScreenSize.GetScreenToWorldHeight / 2;
        m_DownPosition = m_TopPosition - ScreenSize.GetScreenToWorldHeight;
        transform.position = new Vector3(0, m_TopPosition, 0);
        m_YPosition = transform.position.y;
        m_SpriteRenderer.enabled = true;
        //m_FinalAnimation.gameObject.SetActive(true);
    }

    public void MoveToTop()
    {
        m_IsMovingDown = false;
        m_IsMovingUp = true;
    }

    public void MoveToDown()
    {

        m_IsMovingDown = true;
        m_IsMovingUp = false;
    }

    public void ReloadGame()
    {
        GameController.ReloadScene();
    }
}
