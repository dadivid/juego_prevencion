﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {
    public float speedX;
    public float speedY;
    // Use this for initialization
    void Start () {
        
        
        
	}
	
	// Update is called once per frame
	void Update () {
        float offsetX = Time.time * speedX;
        float offsetY = Time.time * speedY;

        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offsetX, offsetY); 
      

    }
}
