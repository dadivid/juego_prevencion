﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static bool m_IsGameActive = true;

    public GenericCharacter m_Player;
    public GenericCharacter m_Monster;

    public Light2D[] m_PlayerLight2D;
    public Light2D m_MonsterLight2D;

    public GameObject m_FinalMessageWin;
    public GameObject m_FinalMessageLose;

    public GameObject[] m_Kids;
    public GameObject[] m_KidsAvatar;

    public static void ReloadScene()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene("CharacterSelect");
    }

    // Start is called before the first frame update
    void Start()
    {
        m_Kids[PlayerPrefs.GetInt("genre")].SetActive(true);
        m_KidsAvatar[PlayerPrefs.GetInt("genre")].SetActive(true);
        m_IsGameActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            ReloadScene();
        }
    }

    public void FinishGame()
    {
        m_IsGameActive = false;
    }

    public void DamagePlayer()
    {
        m_Player.ReduceHealth();
        m_Monster.IncreasePower();
        m_Monster.ActiveAnimation("laugh2", null);
        m_Monster.ModifyAlphaColor(m_Player.m_Health, m_Player.GetInitialHealth());
    }

    public void DamageMonster()
    {
        m_Player.IncreasePower();
        m_Monster.ReduceHealth();
        m_Monster.ActiveAnimation("angry2", null);
    }

    public void ActiveFinalMessage(bool isWinner)
    {
        if(isWinner)
        {
            m_FinalMessageWin.SetActive(true);
        }
        else
        {
            m_FinalMessageLose.SetActive(true);
        }
    }

}
