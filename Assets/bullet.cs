﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{

    Rigidbody2D m_RB;
  
    // Start is called before the first frame update
    void Start()
    {
        m_RB = GetComponent<Rigidbody2D>();
        m_RB.AddForce(transform.up * 10, ForceMode2D.Impulse);
    }

    void Update()
    {
      
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z * -1);
        m_RB.AddForce(transform.up * 10, ForceMode2D.Impulse);
    }
}
